address_data <- read.csv(header = TRUE, sep = "", stringsAsFactors = TRUE, text = '
"Name" "Office"
"Stephan Struckmann" "42"
"Carsten O. Schmidt" "99"
"Adrian Richter" "88"
')
View(address_data)

target_address_data <- read.csv(header = TRUE, sep = "", stringsAsFactors = TRUE, text = '
"First Name" "Middle Name" "Last Name" "Office"
"Stephan" "" "Struckmann" "42"
"Carsten" "O." "Schmidt" "99"
"Adrian" "" "Richter" "88"
')
View(target_address_data)

##### extract titles ######

gsub("^\\s*([a-z]+)\\s+((?:[a-z\\.]+\\s+)*)([a-z]+)\\s*$", 
     "\\1", 
     address_data$Name, 
     ignore.case = TRUE, 
     perl = TRUE)

trimws(gsub("^\\s*([a-z]+)\\s+((?:[a-z\\.]+\\s+)*)([a-z]+)\\s*$", 
     "\\2", 
     address_data$Name, 
     ignore.case = TRUE, 
     perl = TRUE))

gsub("^\\s*([a-z]+)\\s+((?:[a-z\\.]+\\s+)*)([a-z]+)\\s*$", 
     "\\3", 
     address_data$Name, 
     ignore.case = TRUE, 
     perl = TRUE)
