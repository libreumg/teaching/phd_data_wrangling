# References

* [R for data science](https://r4ds.had.co.nz/)
* [Advanced R](https://adv-r.hadley.nz/)
* [R for econometrics](https://www.econometrics-with-r.org/)
* [R markdown](https://bookdown.org/yihui/rmarkdown/)
* [R blogdown](https://bookdown.org/yihui/blogdown/)
* [R bookdown](https://bookdown.org/yihui/bookdown/)

