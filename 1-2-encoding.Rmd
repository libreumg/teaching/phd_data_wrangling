# Encoding (1)

In some cases and when importing data containing German language, so-called encoding issues may arise. For example, you might see data elements (variables) containing text like *KÃ¶rperregion*

```{r}
a <- "KÃ¶rperregion"
a

a <- iconv(iconv(a, "utf8", "latin1"), "utf8", "latin1") # on a mac/linux

# a <- iconv(a, "utf8", "latin1") # on windows
a
```
Such issues can often be fixed very quickly.

# Encoding (2)

-   Computers can... compute, i.e. work with numbers.

-   Text is encoded using numbers

    -   American Standard Code for Information Interchange *ASCII*

```{r}
ASCII <- data.frame(Code = 65:126, 
                         Character = strsplit(rawToChar(as.raw(65:126)), ""))
colnames(ASCII)[2] <- "Character"

DT::datatable(ASCII)

```

-   Problem: Special characters, language dependent

    -   old solution (still 1 byte per character): Encoding of the character codes above \#127 differs depending on the language settings of the computer
    -   new solution (October 1991): Unicode (1-4 bytes per character), `r 2^32` codes become available, and so do Chinese, Arabic, Jewish, ..., Mathematical, Smiley characters. Please find further details about UTF8, UTF16 yourself

-   For further information see: <https://support.rstudio.com/hc/en-us/articles/200532197-Character-Encoding>

*Hint:* Especially Windows has problems with Unicode, still (!).

Unicode literals:

```{r}
"\U1F600"
```

The encoding is tried to be auto-detected from the numbers considered to be text:

```{r}
Encoding("ö")
```

However, this is not always unambiguously possible:

```{r}
Encoding("a")
```

The encoding can also be set:

```{r}
a <- "ö"
a
Encoding(a)
Encoding(a) <- "latin1"
a
Encoding(a)
```

```{r}
c("\U00F6", "o\U0308")
"\U00F6" == "o\U0308"
```


# Encoding (3)

To check for respective encoding issues one option is to use the R-function `View()`:

```{r}
View(sd1b)
```

However, this might be tedious if we have a lot of columns so we can select only relevant data elements:

```{r}
View(sd1b[, sapply(sd1b, is.character)])
```

In this example some R-specifics have been used:

-   we used [indexing](https://cran.r-project.org/doc/manuals/r-release/R-intro.html#Index-vectors) of objects

-   we used on function of the [apply-function family](https://www.r-bloggers.com/2015/07/r-tutorial-on-the-apply-family-of-functions/)

