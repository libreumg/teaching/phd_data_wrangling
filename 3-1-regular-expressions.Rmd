# Handling free-text (2)

## Regular expressions in a nutshell
*RegExps*

-   Term by Stephen C Kleene 1956, "regular language"
-   describe search patterns
-   most simple variant from the command line shells using '\*' and '?' as placeholders, e.g. `*.docx` for anything ending with `.docx` or `?.jpeg` for \# all files with only one-letter names and the file-name extension `.jpeg`.

## More sophisticated *POSIX* RegExps 
*using the* ***PERL*** *extensions*

-   in *R* using `grep`, `grepl`, `gsub`, `sub`, ...

    -   use the argument `perl = TRUE` usually to enable the extended engine, the `perl = FALSE` variant shows strange behaviour in more complex cases

-   <https://regex101.com/>

## The most important patterns

- `*` -- the preceding character can occur 0-$\infty$ times.
- `+` -- the preceding character must occur 1-$\infty$ times.
- `{5}` -- the preceding character must occur 5 times.
- `{5,17}` -- the preceding character must occur 5-17 times.
- `^` -- begin of the string/line
- `$` -- end of the string/line
- `|` -- or, e.g. `(male|female|other)` would match either of these three words

## Group characters with brackets

`(Science)+` -- once or more times the word *Science*

## "Escape" special characters 
*to use them literally*

`\(+` -- one ore more brackets *(*

*Hint:* In *R*, you have to double the backslash, because it already has a special meaning in R character strings.

## Character classes

- `.` -- any character
- `[0-9]` -- any numerical character
- `[a-zA-Z]` -- any letter
- `\s` -- white-space

## Search and Replace

- `sub` and `gsub` can replace pattern matches.

*Hint:* you can refer to all `()`-embraced groups using their order number in the expression:

```{r}
gsub("a(.)f", "\\1", "Heuhaufen")
```

```{r}
gsub("a(.)f", "\\1", c("Heuhaufen", "Heuhaufen2", "Heuhaufen3"))
```

# Handling free-text (3)

## Real-world examples:

-   Normalize white-space:

```{r}
gsub("\\s+", " ", 
     "Stephan    Struckmann, Adrian\tRichter, Carsten\nOliver\n\nSchmidt")
```

-   Detect zip code: mit ^ und $ und regx101 erklären.

```{r}
grepl("([A-Z]\\-)?[0-9]{4,5}",
      c("17489", "D-17489", "xyz", "987654", "6305", "W-6405"))
```

## `soundex`

To find names in databases, long ago algorithms have been developed e.g. for call-centers. The initial version is named `soundex`, and you can compare match strings based on similar "sound". Similarly, the Levenshtein distance (`adist`) can be used.

```{r}
library(stringdist)
phonetic(c("Stefan", "Steffen", "Stephan", "Bill Gates"))

adist("Stephan", c("Stefan", "Steffen", "Stephan", "Bill Gates"))
```

