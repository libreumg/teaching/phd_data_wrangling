# Data wrangling with R ----------------------------------------------------------------------------
# Author: AR
#
# Scope:  This script retrieves the final status of data of day 1 and 2
#
# Job:    Please run the script before the course of day 3 starts and use it as backup, if ,e.g.,
#         the R session crashes during the course
# prepare session ----------------------------------------------------------------------------------

# create the directory is necessary
data_wrangling_home <- file.path(path.expand("~"), "data_wrangling")
if (!dir.exists(data_wrangling_home)) {
 dir.create(data_wrangling_home, recursive = TRUE)
}
cat("Directory for course material:", data_wrangling_home)

# set working directory
setwd(data_wrangling_home)

# day 1 --------------------------------------------------------------------------------------------
# load data
sd1a <- readRDS("data/studydata.RDS")

# correct date of visit_0
sd1a$visit_0 <- gsub(" ", "-", sd1a$visit_0) # replace "blank" by "-" in visit_0

# assign date format
sd1a$visit_0 <- as.Date(sd1a$visit_0, format = "%Y-%m-%d")

# correct long integer of bio_mat_ID_0
sd1a$bio_mat_ID_0 <- format(sd1a$bio_mat_ID_0, scientific = FALSE)

# day 2 --------------------------------------------------------------------------------------------

# CREATE DD

# function to create a small DD from a dataframe
mysmallDD <- function(my_df) {
 
 whichlevels <- function(z) {
  if(is.factor(z)) {
   if(length(levels(z)) > 5) {c(levels(z)[1:5], "and further")}
   else levels(z)
  } else {
   NA
  }
 }
 
 minstats <- function(y) {
  if(is.numeric(y) | is.factor(y)) {
   if(is.factor(y) & length(levels(y) < 11)) {
    desc_stats <- table(y)
    paste0(names(desc_stats), " = ", as.character(desc_stats)) 
   } else {
    desc_stats <- round(summary(y)[c(1, 3, 6)], digits = 3)
    paste0(names(desc_stats), " = ", as.character(desc_stats)) 
   }
  } 
  else {
   NA
  }
 }
 
 n_miss <- function(f) {
  N_MISSING <- sum(is.na(f))
 }
 
 # initialize DF
 xdd <- data.frame(VAR_NAMES = names(my_df),
                   DATA_TYPE = as.character(sapply(my_df, class)))
 
 # summarize minimum statistics and factor levels
 xdd$FACTOR_LEVELS <- as.character(sapply(my_df, whichlevels))
 xdd$DESCRIP_STATS <- as.character(sapply(my_df, minstats))
 xdd$`N Missing` <- sapply(my_df, n_miss)
 
 # format
 rownames(xdd) <- NULL
 xdd$FACTOR_LEVELS <- gsub("c\\(|\\)", "", xdd$FACTOR_LEVELS)
 xdd$DESCRIP_STATS <- gsub("c\\(|\\)", "", xdd$DESCRIP_STATS)
 
 # add a description columns
 xdd$DESCRIPTION <- NA
 
 return(xdd)
 
}

sd1a$Sex <- factor(sd1a$Sex)

# apply function to sd1a to create a small dd
sd1a_dd <- mysmallDD(sd1a)

# STANDARDIZE DRUGS

# Find all drug variables using grep:
all_drug_vars <- grep("^drug_", # starts with drug_
                      colnames(sd1a), # do for the variable names
                      value = TRUE) # return the matched values, not its indexes

# define a function to normalize a vector of drug names
# there are easier, but less instructive ways to do so
normalize_drug <- function(d0) {
 return(gsub(
  "^.*(Zebzitabin|Bazopamab).*$", # search for the drug names as group 1
  "\\1", # replace the matched string (.* ensures, the full string) by group 1
  d0, # search in d0
  perl = TRUE, # use the PERL engine
  ignore.case = TRUE)) # ignore capitalization
}

# get only the drug columns -- for better readability of the code
drug_data <- sd1a[, all_drug_vars]

# use an apply function to normalize each column in kind of a loop 
# (never use for-loops in functional R for performance reasons)
drug_data <- 
 lapply(drug_data, normalize_drug) # data.frames are lists of columns

sd1a[, all_drug_vars] <- drug_data # write the modified data back


# discretize continuous variable
# check if arules is already installed
if (!("arules" %in% installed.packages())){
 install.packages("arules")
}

sd1a$DAS_DA_GROUP_0 <- arules::discretize(sd1a$das_0,
                                          method = "fixed",
                                          breaks = c(-Inf, 2.6, 3.2, 5.1, Inf),
                                          right = TRUE,
                                          labels = c("Remission", "Low DA", "Moderate DA", "High DA"))


# recode a categorical variable from 4 to 3 levels
# copy to new variable
sd1a$EULAR_DA_GROUP_0 <- sd1a$DAS_DA_GROUP_0

# re-level
levels(sd1a$EULAR_DA_GROUP_0) <- c("Low DA", "Low DA", "Moderate DA", "High DA")


# clear session (optional)
#rm(list = ls()[!(ls() %in% c("sd1a", "data_wrangling_home"))])
