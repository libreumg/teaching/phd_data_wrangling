# Data dictionary (DD) (1)

In fact, there is no harmonized definition of the term *data dictionary*. A working definition is:

-   *"We use the term Element Data Dictionary (or element dictionary in shorter form) to describe a spreadsheet that enumerates individual data elements used in a study with fields such as data element name and description" (Mayer et. al*$^1$)

$^1$ Mayer, Craig S., Nick Williams, and Vojtech Huser. "Analysis of data dictionary formats of HIV clinical trials." Plos one 15.10 (2020): e0240047.

DD should be used to annotate data; for the comprehensibility of the data and for everyone dealing with the data.

Example for different granularity of descriptions:

-   CRP \> 0.5 = c-reactive protein
-   hsCRP \> 0.5 = high sensitive c-reactive protein, binary
-   hsCRP (mg/dL) \> 0.5 = high sensitive c-reactive protein, binary

It is advisable to add all relevant description to the data dictionary. For example, the DD could also contain the whole question that has been asked, such as "Did you have backpain in the last 3 month (yes/no)?".


# Data dictionary (DD) (2)
## Import DD/metadata (Exercise)

```{r}
sd1_dd <- openxlsx::read.xlsx(xlsxFile = "data/dwwr_dd.xlsx")
```

-   VAR_NAMES: denotes the name of the data element
-   DATA_TYPE: denotes the expected data type of the data element\
-   VALUE_LABELS: if applicable, values and levels of categorical variables
-   HARD_LIMITS: if applicable, limits for data elements of type float

# Data dictionary (DD) (3)
## Compare studydata and metadata

We can make use of the R package `dataquieR` which has an embedded function that compares study data and metadata

```{r}
library(dataquieR)
# apply applicability plot
appmatrix <- dataquieR::pro_applicability_matrix(study_data = sd1a,
                                                 meta_data = sd1_dd)
```

```{r fig.height=10}
# plot
appmatrix$ApplicabilityPlot
str(sd1a$bio_mat_ID_0)
```

Illustration using the initially imported data:

```{r fig.height=10}
sc1 <- readRDS("data/studydata.RDS")
pasc1 <- dataquieR::pro_applicability_matrix(study_data = sc1,
                                             meta_data  = sd1_dd)
pasc1$ApplicabilityPlot
```

# Data dictionary (DD) (4)
## Create a small DD

We can apply the function(s) shown below to create a data dictionary on our own:

```{r}
# function to create a small DD from a data frame
mysmallDD <- function(my_df) {
  
  whichlevels <- function(z) {
    if(is.factor(z)) {
      if(length(levels(z)) > 5) {
        paste0(paste0(levels(z)[1:5], collapse = " | "), " + ", length(levels(z)) - 5," further", collapse = " | ")      
      } else {
        paste0(levels(z), collapse = " | ")
      }
    } else {
      NA
    }
  }
  
  minstats <- function(y) {
    if(is.numeric(y) | is.factor(y)) {
      if(is.factor(y) & length(levels(y) < 11)) {
        desc_stats <- table(y)
        paste0(names(desc_stats), " = ", as.character(desc_stats)) 
      } else {
        desc_stats <- round(summary(y)[c(1, 3, 6)], digits = 3)
        paste0(names(desc_stats), " = ", as.character(desc_stats)) 
      }
    } 
    else {
      NA
    }
  }
  
  n_miss <- function(f) {
    N_MISSING <- sum(is.na(f))
  }
  
  coll_desc <- function(x) {
    paste0(x, collapse = " | ")
  }
  
  # initialize DF
  xdd <- data.frame(VAR_NAMES = names(my_df),
                    LABEL     = ifelse(is.null(attributes(my_df)$variable.labels), 
                                       NA,
                                       attributes(my_df)$variable.labels),
                    DATA_TYPE = as.character(sapply(my_df, class)))
  
  # summarize minimum statistics and factor levels
  xdd$FACTOR_LEVELS <- as.character(sapply(my_df, whichlevels))
  xdd$DESCRIP_STATS <- as.character(lapply(sapply(my_df, minstats), coll_desc))
  xdd$`N Missing` <- sapply(my_df, n_miss)
  
  # format
  rownames(xdd) <- NULL

  # add a description columns
  xdd$DESCRIPTION <- NA
  
  return(xdd)
  
}
```

<br>

Creating an example data dictionary with some description:

```{r}
# apply function to sd1a
sd1a_dd <- mysmallDD(sd1a) 
# View
View(sd1a_dd)
```