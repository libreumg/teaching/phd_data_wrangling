#---------------------------------------------------------------------------------------------------
# Data Wrangling: Simulated data example
#
# Author: Adrian Richter
# Date:   2021-04-29
# Packages
library(MASS)
library(lqmm)
library(lubridate)
library(plyr)         
library(dplyr)        

# PSEUDO-ID ----------------------------------------------------------------------------------------
# integer part
sdf <- data.frame(int_part = paste0(sample(0:9, size = 500, replace = TRUE), 
                                    sample(0:9, size = 500, replace = TRUE), 
                                    sample(0:9, size = 500, replace = TRUE)))

# character part
sdf$ID <- NA
for (i in 1:dim(sdf)[1]) {
  set.seed(i + 11235)
  sdf$ID[i] <- paste0(paste0(LETTERS[sample(1:26, 5, replace = TRUE)],  collapse = ""), 
                      sdf$int_part[i])
}

# add pseudo-ID to df
sdf <- sdf[, "ID", drop = FALSE]

# add sex
sdf$Sex <- rbinom(n = 500, size = 1, prob = 0.7)          

# add age
sdf$Age <- round(rnorm(n = 500, mean = 55, sd = 10), digits = 0)

# Bio material ID
sdf$bio_mat_ID_0 <- sample(20181100270000:20181100275000, 500, replace = FALSE)

# Follow-Up dates --------------------------------------------------------------
set.seed(11235)
sdf$visit_0 <- as.Date(as.POSIXct(seq(0, 200, length = 500) * 3600 * 24, 
                                  origin = as.Date("2018-01-01")), "%Y-%m-%d")

sdf$visit_1 <- sdf$visit_0 + round(rnorm(500,    90, 10), digits = 0)
sdf$visit_2 <- sdf$visit_0 + round(rnorm(500, 182.5, 10), digits = 0)
sdf$visit_3 <- sdf$visit_0 + round(rnorm(500,   365, 10), digits = 0)
sdf$visit_4 <- sdf$visit_0 + round(rnorm(500, 547.5, 10), digits = 0)
sdf$visit_5 <- sdf$visit_0 + round(rnorm(500,   730, 10), digits = 0)

# create issue
sdf$visit_0 <- as.character(sdf$visit_0)
whichone <- sample(1:500, 3)
sdf$visit_0[whichone] <- gsub("-", " ", sdf$visit_0[whichone])

# Disease activity -------------------------------------------------------------
# mean DAS28
mu_das <- c(5, 4, 3, 3, 2.8, 2.8)

# definition of a covariance matrix which defines covariance structure (association)
Sigma <- matrix(c(1.4, 1.1, 0.5,   0,   0,   0, 
                  1.1, 1.2, 0.8, 0.5,   0,   0,
                  0.5, 0.8,   1, 0.7, 0.4,   0,
                    0, 0.5, 0.7, 0.8, 0.6, 0.2,
                    0,   0, 0.4, 0.6, 0.7, 0.4,
                    0,   0,   0, 0.2, 0.4, 0.5)
                , 6, 6)

# positive definite covariance?
Sigma <- make.positive.definite(Sigma)

# create empty columns
for (i in 0:(length(mu_das) - 1)) {
  sdf[paste0("das_", i)] <- NA
}

# fill with simulated data from multivariate normal distribution
set.seed(20191018)
sdf[, grepl("das", names(sdf))] <- mvrnorm(n = dim(sdf)[1], mu = mu_das, Sigma = Sigma)

# round das values
sdf[, grepl("das", names(sdf))] <- apply(sdf[, grepl("das", names(sdf))], 2, 
                                         round, 
                                         digits = 2) 

# HAQ --------------------------------------------------------------------------
# mean DAS28
mu_haq <- c(1.3, 1.4, 1.5, 1.5, 1.5, 1.6)

# definition of a covariance matrix which defines covariance structure (association)
Sigma <- matrix(c(0.23,  0.16,    0,    0,    0,    0, 
                  0.16,   0.2, 0.15,    0,    0,    0,
                     0,  0.15, 0.10, 0.05,    0,    0,
                     0,     0, 0.05, 0.10, 0.05,    0,
                     0,     0,    0, 0.05, 0.10, 0.05,
                     0,     0,    0,    0, 0.05, 0.10)
                , 6, 6)

# positive definite?
Sigma <- make.positive.definite(Sigma)

# create empty columns
for (i in 0:(length(mu_haq) - 1)) {
  sdf[paste0("haq_", i)] <- NA
}

# fill with simulated data from multivariate normal distribution
set.seed(20191018)
sdf[, grepl("haq", names(sdf))] <- mvrnorm(n = dim(sdf)[1], mu = mu_haq, Sigma = Sigma)

# transform the few negative values
sdf[, grepl("haq", names(sdf))] <- apply(sdf[, grepl("haq", names(sdf))], 2, abs) 

# round haq values
sdf[, grepl("haq", names(sdf))] <- apply(sdf[, grepl("haq", names(sdf))], 2, 
                                         round, 
                                         digits = 2) 

# Treatment --------------------------------------------------------------------
set.seed(11235)

ther_mat <- matrix(sample(c("Zebzitabin", "Zebzitabin-Mono", "Zebzitabin mono", "Zebzitabin 600 mg/m", 
                            "Bazopamab", "Bazopamab 800 mg daily", "Bazopamab 800mg/täglich","Bazopamab mono"), 
                          3000, 
                          replace = TRUE,
                          prob = c(0.46, 0.01, 0.01, 0.01,
                                   0.46, 0.01, 0.01, 0.01)),
                   ncol = 6)

ther_mat <- as.data.frame(ther_mat)
names(ther_mat) <- paste0("drug_", 0:5)

sdf <- cbind(sdf, ther_mat)


# COPD depending on HAQ --------------------------------------------------------
# mean haq
mean_haq <- apply(sdf[, grepl("haq", names(sdf))], 1, mean)

# probability depending on haq
copd_p <- exp(0.01 - 1.5*mean_haq + 0.02*sdf$Age) / (1 + exp(0.01 - 1.5*mean_haq + 0.02*sdf$Age))

# function to generate random COPD based on individual probabilities
set.seed(11235)
copd <- rbinom(500, 1, prob = copd_p)

# when was COPD reported?
fu <- sample(0:5, 500, replace = TRUE)
fu_copd <- ifelse(copd == 1, fu, copd)

sdf$copd_0 <- ifelse(fu_copd == 0 & copd == 1, 1, 0)
sdf$copd_1 <- ifelse(fu_copd == 1 | sdf$copd_0 == 1, 1, 0)
sdf$copd_2 <- ifelse(fu_copd == 2 | sdf$copd_1 == 1, 1, 0)
sdf$copd_3 <- ifelse(fu_copd == 3 | sdf$copd_2 == 1, 1, 0)
sdf$copd_4 <- ifelse(fu_copd == 4 | sdf$copd_3 == 1, 1, 0)
sdf$copd_5 <- ifelse(fu_copd == 5 | sdf$copd_4 == 1, 1, 0)

sdf[, grep("copd", names(sdf))] <- apply(sdf[, grep("copd", names(sdf))], 2, as.numeric)

# create missingness -------------------------------------------------------------------------------
# mean das at baseline and 1st follow-up
mean_das_0_to_1 <- apply(sdf[, c("das_0", "das_1")], 1, mean)

# probability depending on DAS28 and inclusion date
month <- month(as.Date(sdf$visit_1))
miss_p <- exp(0.01 + 0.6*mean_das_0_to_1 + 0.1*month) / 
 (1 + exp(0.01 + 0.6*mean_das_0_to_1 + 0.1*month))

miss_p
# function to generate missings based on individual probabilities
set.seed(11235)
missi <- rbinom(500, 1, prob = miss_p/2)

# when was 1st missing?
fu <- sample(1:5, 500, replace = TRUE)
sdf$fu_miss <- ifelse(missi == 1, fu, missi)


# insert monotone missingness

for (i in 1:5) {

 # from 1st FU with missing to max FU
 fuexpr <- paste0("_", i:5, collapse = "|")
 
 # dimensions
 howmany <- dim(sdf[sdf$fu_miss == i, names(sdf)[grep(fuexpr, names(sdf))]])
 
 # create respective NAs
 NA_mat <- matrix(NA, ncol = howmany[2], nrow = howmany[1])
 
 # insert NAs
 sdf[sdf$fu_miss == i, names(sdf)[grep(fuexpr, names(sdf))]] <- NA_mat
 
}


sdf$fu_miss <- NULL

# create longitudinal duplicates -------------------------------------------------------------------
# only for two observations
sdf$visit_4[sdf$ID == "ZPHBZ053"] <- sdf$visit_3[sdf$ID == "ZPHBZ053"]
sdf$das_4[sdf$ID == "ZPHBZ053"] <- sdf$das_3[sdf$ID == "ZPHBZ053"]
sdf$haq_4[sdf$ID == "ZPHBZ053"] <- sdf$haq_3[sdf$ID == "ZPHBZ053"]


sdf$visit_5[sdf$ID == "AVAEH041"] <- sdf$visit_4[sdf$ID == "AVAEH041"]
sdf$das_5[sdf$ID == "AVAEH041"] <- sdf$das_4[sdf$ID == "AVAEH041"]
sdf$haq_5[sdf$ID == "AVAEH041"] <- sdf$haq_4[sdf$ID == "AVAEH041"]




# reorder columns according to visits --------------------------------------------------------------
sdf <- sdf[, c("ID", "Sex", "Age", 
               names(sdf)[grep("_0", names(sdf))],
               names(sdf)[grep("_1", names(sdf))],
               names(sdf)[grep("_2", names(sdf))],
               names(sdf)[grep("_3", names(sdf))],
               names(sdf)[grep("_4", names(sdf))],
               names(sdf)[grep("_5", names(sdf))])]


# SAFETY DATA --------------------------------------------------------------------------------------
# ID
set.seed(20191018)
safetydf <- data.frame(ID = sample(sdf$ID, 200, replace = TRUE))

# Event date
safetydf$event_date <- min(as.Date(sdf$visit_1), na.rm = TRUE) + sample(1:500, 200, replace = TRUE)
safetydf <- safetydf[ order(safetydf$ID, safetydf$event_date), ]


# Event-number
# This should be part of the solutions
safetydf <- ddply(safetydf, .(ID), mutate, event_nr = seq_along(event_date))

# Event type
safetydf$event_type <- "SI"

# select thos with events
testsf <- safetydf[safetydf$event_nr ==1,]
testsdf <- sdf[sdf$ID %in% testsf$ID,]
# combine
testboth <- merge(testsdf, testsf, by = "ID", all.x = TRUE)

# select IDs
whichnot <- testboth$ID[testboth$event_date<testboth$visit_0]
safetydf <- safetydf[!(safetydf$ID %in% whichnot), ]
safetydf$event_nr <- NULL


# save study data ----------------------------------------------------------------------------------

saveRDS(sdf, file = "C:/Users/richtera/Documents/ShareRichterA/Lehre/PHD Programme/studydata.RDS")
saveRDS(safetydf, file = "C:/Users/richtera/Documents/ShareRichterA/Lehre/PHD Programme/safetydata.RDS")

openxlsx::write.xlsx(sdf, 
                     file = "C:/Users/richtera/Documents/ShareRichterA/Lehre/PHD Programme/studydata.xlsx")
