# Date formats: Exercise (1)

Please the following code to create a vector in R:

```{r}
set.seed(1)
mydates <- paste0(sample(1:30, 10, replace = TRUE),
                  ".",
                  sample(1:12, 10, replace = TRUE),
                  ".",
                  seq(2013,2022, 1))
```

- check the class of this vector using `class()`

- visit the R-bloggers [date format guide](https://www.r-bloggers.com/2019/04/the-ultimate-opinionated-guide-to-base-r-date-format-functions/) and try to specify the correct format of this vector. In particular this figure:

![Abbreviations used for date formats](figures/05-Date-Format-Abbreviations.png)

- create a new vector `mydatesf` which uses `mydates` as input but use now `as.Date()` and specify the format

- combine both vectors into a data frame and print it


# Date formats: Exercise (2)

This exercise refers to the previously imported data frames `sd1a` and `sd1b`.

## Task 

- Please compare the content of both data frames using the R function `str()`.

- Whats different?

```{r}
str(sd1a)
str(sd1b)
```

-   in `sd1a` four data elements are of type *date*
-   in `sd1b` none of the data elements is of type *date*

# Date formats: Exercise (3)

Please see the documentation of the R package `openxlsx` using `?openxlsx::read.xlsx`.

# Date formats: Exercise (4)

This function has a formal `detectDates` which is set to `FALSE`.

```{r}
sd1c <- openxlsx::read.xlsx(xlsxFile = "data/studydata.xlsx",
                            sheet = 1,
                            detectDates = TRUE)

all.equal(sd1a, sd1c)
```

<br>

Both data frames are now identical. However, there is still one issue:

```{r}
str(sd1a$visit_0)
```

**CAVE**:

-   the import routine has been working for other data elements to correctly import the data with data types *date*
-   apparently this is not working for this column

We will check for this common issue/pitfall in the next section which inspects the *__structure__* of the data. This is different from pitfalls in which the *__content__* of the data causes importing issues.